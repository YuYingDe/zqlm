/**
 * Created by YYD on 4/11/16.
 */
module.exports = {
    entry: "./main.js",
    output: {
        path: "./build",
        publicPath: "/build/",
        filename: "build.js"
    },
    module: {
        loaders: [
            {test: /\.vue$/, loader: 'vue'},
            { test: /\.styl$/, loader: "style!css!stylus" },
            { test: /\.html$/, loader: "html" }
        ]
    }
};
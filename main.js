/**
 * Created by YYD on 4/11/16.
 */
var Vue = require('vue');
var VueRouter = require('vue-router');
Vue.use(VueRouter);

var App = Vue.extend({})

var router = new VueRouter();

router.map({
    '/': {
        component: require('./template/main.vue')
    }
})

router.start(App, '#app');